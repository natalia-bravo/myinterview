USE employees; 

-- Query que retorna a quantidade de funcionários separados por sexo.
SELECT gender, count(gender) as quantidade 
	FROM employees
	GROUP BY gender;

-- Query que retorna a quantidade de funcionários distintos por sexo, ano e ano de nascimento.
SELECT count(emp_no) as quantidade, gender, date_format(birth_date, '%Y') as birth_year, date_format(hire_date, '%Y') as hire_year
	FROM employees
	GROUP BY gender, date_format(birth_date, '%Y'), date_format(hire_date, '%Y');
    
-- Query que retorna a média, min e max de salário por sexo.
SELECT e.gender as sexo, avg(s.salary) as media, min(s.salary) as minimo, max(s.salary) as maximo
	FROM employees e INNER JOIN salaries s
    ON e.emp_no = s.emp_no
    GROUP BY e.gender;