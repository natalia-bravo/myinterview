package com.example;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import org.json.JSONArray; 
import org.json.JSONException; 
import org.json.JSONObject;

/**
 * Create an implementation of a Rest API client.
 * Prints out how many records exists for each gender and save this file to s3 bucket
 * API endpoint=> https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda 
 * AWS s3 bucket => interview-digiage
 *
 */
public class TASK4 {
    private static String URL = "https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda";

    private static TASK4 instance;
    private CloseableHttpClient clientHTTP;
    
    private TASK4(){
        this.clientHTTP = HttpClients.createDefault();
    }
    
    public static TASK4 getInstance() {
        if(instance == null) {
            instance = new TASK4();
        }
        return instance;
    }
    
    public String doRequest() {
        String responseBody = null;
        
        try {
            HttpGet httpGet = new HttpGet(TASK4.URL);
            
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException,
                        IOException {
                    int status = response.getStatusLine().getStatusCode();
                    
                    if(status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    }
                    else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }
            };
            responseBody = this.clientHTTP.execute(httpGet, responseHandler);
        }
        catch (IOException ex) {
            Logger.getLogger(TASK4.class.getName()).log(Level.SEVERE, null, ex);
        }
        return responseBody;        
    }

    public static void main(String[] args) throws JSONException {
        TASK4 instance = TASK4.getInstance();
        int m = 0, f= 0;

        String str = instance.doRequest();

        JSONArray json = new JSONArray(str);

        for(int i=0; i < json.length(); i++) {  
            JSONObject object = json.getJSONObject(i);  
            if(object.getString("gender").equals("M")) {
                m++;
            }
            else {
                f++;
            }
        }
        System.out.println("Quantidade de homens: " + m + ". Quantidade de mulheres: " + f);

    }

}