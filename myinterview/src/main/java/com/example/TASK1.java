package com.example;
import java.util.Scanner;
/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite uma palavra:");
        String word = sc.nextLine();
        sc.close();

        if(isPalindrome(word)) System.out.print(word + " é palíndromo");
        else System.out.print(word + " não é palíndromo");
        
    }

    private static boolean isPalindrome(String str) {
        String copy = str.replace(" ", "");
        int tam = copy.length();
        for(int i = 0; i < tam/2; i++) {
            if(copy.charAt(i) != copy.charAt(tam-i-1)) return false;
        }
        return true;
    }
 
}